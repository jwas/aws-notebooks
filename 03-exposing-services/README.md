Exposing services
=================

After finishing this course, you should know how to:
* delegate a domain to AWS nameservers
* create an A record and point it to an EC2 instance
* configure TLS to secure your app
* use an application load balancer (with TLS)

## Overview

[Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)
is a service to manage your domains and configure them to direct traffic
to other AWS services. Domains can be registered in Route 53 or delegated from other registrars.

[TLS](https://www.cloudflare.com/learning/ssl/transport-layer-security-tls/)
is a popular protocol to secure traffic and verify identity.
Most modern applications use it, even in development environments.
Thanks to services like [Let's Encrypt](https://letsencrypt.org/getting-started/),
which provide free, automatically reneable TLS certificates to anyone,
there are no reasons not to secure your applications in every environment.

Some applications, like the [Caddy](https://hub.docker.com/_/caddy) HTTP server,
integrate with Let's Encrypt and allow to request TLS certificates with very little configuration.

[AWS Certificate Manager](https://docs.aws.amazon.com/acm/latest/userguide/acm-services.html)
integrates with other AWS services, like
[Elastic Load Balancing](https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/what-is-load-balancing.html)
and also allow to use free TLS certificates.


## Excercise

If you own a domain, registered outside of AWS, you can delegate it to AWS to manage it there.
To do that, create a new hosted zone in the Route 53 service:
```bash
aws route53 create-hosted-zone --name your-domain --caller-reference "$(date)"
```

If you don't own a domain, you can use `infotek.app`, which already has a hosted zone created,
and is delegated to AWS name servers.

```bash
aws route53 list-hosted-zones
```

To create an A record, pointing to an EC2 instance's public IP address, edit the provided `change.json` file
and set:
* the `ResourceRecordSet.Name` field to a unique subdomain
* the `ResourceRecords.Value` to the public IP address of your EC2 instance

Then apply it with:
```bash
aws route53 change-resource-record-sets --hosted-zone-id ZXXXXXXXXXX --change-batch file://change.json
```

After verifying in your browser that the subdomain works, you can now try to run a secure HTTP server with TLS enabled.
Log into your server and stop the Nginx container, created in the previous course, if it's still running:

```bash
docker stop app
docker rm app
```

Run Caddy, with TLS enabled:
```bash
docker run --name app -d -p 80:80 -p 443:443 \
    -v /site:/srv \
    -v caddy_data:/data \
    -v caddy_config:/config \
    caddy \
    caddy file-server --domain a.infotek.app
```

Make sure to open up the HTTPS port (443) for incoming traffic from any IP, because Caddy will ask Let's Encrypt servers
to verify the domain ownership before it can issue a signed TLS certificate.

## Test

1. Can an A record point to more than one IP? When this would be useful?
1. If you change the IP to which an A record points to, how fast is the change visible to users around the world?
1. What are common attacks that can be prevented with using TLS?

## Extras

Configure an ELB with multiple instances.
