#!/bin/sh

set -eu

if [ -z "${1:-}" ] || [ -z "${2:-}" ]; then
    echo "Usage: $0 <repository-name> <lambda-name>"
    echo ""
    echo "  <repository-name> - repository (image) name"
    echo "  <lambda-name> - AWS Lambda name"
    exit 1
fi

aws() {
    docker run --rm \
        -v $HOME/.aws:/root/.aws \
        -e AWS_ACCESS_KEY_ID \
        -e AWS_SECRET_ACCESS_KEY \
        -e AWS_DEFAULT_REGION \
        amazon/aws-cli \
        "$@"
}

lambda=$2
name=514111127510.dkr.ecr.eu-central-1.amazonaws.com/$1
version=$(git describe --tags --always)

if output=$(aws lambda update-function-code --function-name "$lambda" --image-uri "$name:$version"); then
    echo "$output"
else
    aws lambda create-function --region eu-central-1 --function-name "$lambda" \
        --package-type Image \
        --code ImageUri="$name:$version"  \
        --role arn:aws:iam::514111127510:role/service-role/jwas-role-vt8tmmt9
fi
