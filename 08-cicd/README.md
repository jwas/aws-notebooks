CI/CD
=====

After finishing this course, you should know how to:
* automate running tests and creating releases after pushing to a remote repository
* automate deploying code to a Lambda function

## Overview

Continuous integration (CI) is the practice of merging all developers' working copies
to a shared mainline several times a day.

Typically, this includes the following operations:
* build code
* run automated tests
* create an artifact

Continuous deployment (CD) is a software engineering approach in which software functionalities
are delivered frequently through automated deployments.
CD contrasts with continuous delivery, a similar approach in which software functionalities
are also frequently delivered
and deemed to be potentially capable of being deployed but are actually not deployed.

Gitlab is an example software development platform that has support
for the continuous methodologies: https://docs.gitlab.com/ee/ci/

## Excercise

Start with verifying that pipelines work, by creating one that only prints messages:

1. Create a `.gitlab-ci.yml` file in the root of your local clone of this repository.
   Use the example from Gitlab docs linked in the previous section.
1. Commit the new file to a new branch, and push it to the repository.
1. Observe the pipeline running for the changes on your branch.

Next, execute the `build.sh` and `test.sh` scripts in the right jobs to build and test the Lambda function in a container.

If the test passes, the container image can be pushed to the remote repository and it can be deployed to a Lambda function.

> Note: the AWS credentials required to log into the ECR repository and deploy the lambda
> should be already configured in the repository.
> Run the deploy job using a specific image so the `aws` commands are available there.

```
deploy:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest  # see the note below
  script:
    - ./08-cicd/push.sh
    - ./08-cicd/deploy.sh
```
