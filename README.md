# aws-notebooks

This repository contains notebooks for learning AWS basics.
They are meant to be edited during learning, to take notes and write down exercice results.

* [Intro](00-intro/) - learning goals (scope), conventions used, required tools
* [What is a Cloud](01-what-is-a-cloud/) - overview of Cloud Provider services, using the Web Console, the CLI and what is IAM
* [Compute services](02-compute-services/) - services to run applications
* [Exposing services](03-exposing-services/) - how to make applications available to anyone
* [Automation for EC2](04-ec2-automation/) - automate configuring EC2 instances
* [Serverless](05-serverless/) - run apps without managing servers
* [Deploying Lambdas](06-deploying-lambdas/) - deploy local Python code to AWS Lambda
* [Lambda images](07-lambda-images/) - deploy Lambdas from Docker images
* [CI/CD](08-cicd/) - continuous integration (CI) and continuous deployment (CD)
* [Object storage](09-object-storage/) - simplest and cheapest way to store data
* [Databases](10-databases/) - managing and using key-value and relational databases
* [Automation](11-automation/) - manage cloud resources automatically
