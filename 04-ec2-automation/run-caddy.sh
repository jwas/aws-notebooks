#!/usr/bin/env bash

docker run --name app -d -p 80:80 -p 443:443 \
    -v /site:/srv \
    -v caddy_data:/data \
    -v caddy_config:/config \
    caddy \
    caddy file-server --domain a.infotek.app
