Automation for EC2
==================

After finishing this course, you should know how to:
* create custom Amazon Machine Images (AMIs)
* run custom scripts on instance startup

## Overview

When creating an EC2 instance, you must choose an image (AMI)
from which the root disk of the instance will be created from.
This decides:
* which operating system or particular distribution the instance will use,
* what software will be already installed on the machine,
* and what other data and configuration will be present.

To avoid having to configure every instance after creating it,
[a custom AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html#creating-an-ami)
can be created and used for new instances.

Another way of configuring instances is automatically [running commands
after they start](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html).
Comparing to AMIs, this is useful when using configuration
not known at the time of creating an AMI. But it still takes time
to execute all commands, so using an AMI means that instances are ready faster.

## Excercise

1. Create an instance using the `Amazon Linux 2` AMI (ami-07df274a488ca9195)
1. [install Docker](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html)
   and pull the Caddy server Docker image
1. Create a custom AMI
1. Start an instance using this AMI and start Caddy in a container using a custom domain name
1. Check if the command completed successfuly by reading
   the `/var/log/cloud-init-output.log` file on the instance.

## Test

1. What needs to be included in an AMI to support running commands from user-data?
1. What's the minimum set of arguments for the `aws ec2 run-instances` to start
   an instance with a running application ready to be used?

## Extras

Find and test other tools that allow building custom AMIs.
