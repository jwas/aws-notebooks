Compute Services
================

After finishing this course, you should know how to:
* use the EC2 service to create VM instances
* connect to the VM instance and run your applications on it
* evaluate the performance

## Overview

[The EC2 service](https://aws.amazon.com/ec2/features/) provides Virtual Machines (VMs),
running various OSes, including Linux, Windows and even MacOS.
They are located in geographically diverse regions. They use [logical networking](https://aws.amazon.com/vpc/)
and [network disks](https://aws.amazon.com/ebs/features/).

## Excercise

```bash
# following https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-keypairs.html#creating-a-key-pair
# use a unique key name instead of `default`
# `--query 'KeyMaterial'` together with `--output text` will print the private key, which is redirected to a file
aws ec2 create-key-pair --key-name default --query 'KeyMaterial' --output text > key.pem
# a file created by redirecting output to it has too wide permissions, and the ssh command used later would complain about it
chmod 0600 key.pem

# following https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-sg.html
# capture output of the `aws ec2 describe-vpcs` command into a variable called `vpc`)
vpc=$(aws ec2 describe-vpcs \
    --filters Name=isDefault,Values=true \
    --query "Vpcs[0].VpcId" --output text)

# create a new security group, letting in traffic for 2 ports, SSH (22) and HTTP (80)
aws ec2 create-security-group \
    --vpc-id "$vpc" \
    --group-name infotech \
    --description "Let in SSH and HTTP" \
    --query "GroupId"
# if you get an error saying the group already exists, you can decide to use it anyway

# or just get the security group ID, if it already exists
securityGroup=$(aws ec2 describe-security-groups \
    --group-names infotech \
    --query "SecurityGroups[0].GroupId" --output text)

# make sure the group allows incoming traffic from your IP address
myIp=$(curl https://checkip.amazonaws.com)
aws ec2 authorize-security-group-ingress --group-id "$securityGroup" --protocol tcp --port 22 --cidr "$myIp/32"
aws ec2 authorize-security-group-ingress --group-id "$securityGroup" --protocol tcp --port 80 --cidr "$myIp/32"

# following https://docs.aws.amazon.com/cli/latest/userguide/cli-services-ec2-instances.html#launching-instances
# find all images with name matching `ubuntu*20.04-arm64-server*`, sort them by CreationDate,
# get the last item (newest image) and get its ID
imageId=$(aws ec2 describe-images \
    --filters "Name=name,Values=ubuntu*20.04-amd64-server*" \
    --query "sort_by(Images, &CreationDate)[-1:].ImageId" --output text)
# or use a specific AMI
imageId=ami-05f7491af5eef733a

# finally request a new instance, using the image and security group found earlier and your key
# capture its ID to avoid having to look it up later
instanceId=$(aws ec2 run-instances \
    --image-id "$imageId" \
    --instance-type t2.micro \
    --key-name default \
    --security-group-ids "$securityGroup" \
    --query "Instances[0].InstanceId" --output text)
aws ec2 wait instance-running --instance-ids "$instanceId"

# using the instance ID, describe it and query for the public IP address
ip=$(aws ec2 describe-instances --instance-ids "$instanceId" --query "Reservations[*].Instances[*].PublicIpAddress")
# refresh the packages list, install Docker
ssh -i key.pem ubuntu@"$ip" sudo apt-get update && sudo apt-get install docker.io
# run a web server in a container named app, running in the background, exposing port 80
ssh -i key.pem ubuntu@"$ip" sudo docker run --name app -d -p 80:80 nginx

# benchmark it!
curl -fLO https://github.com/tsenart/vegeta/releases/download/v12.8.4/vegeta_12.8.4_linux_amd64.tar.gz
tar xf vegeta_12.8.4_linux_amd64.tar.gz vegeta
echo "GET http://$ip/" | ./vegeta attack -duration=5s | ./vegeta report
```

## Test

1. How to choose the region and the availability zone?
1. When can the external IP of a VM change?
1. What's an AMI?
1. What can influence how many requests per second can the app hangle?
