Deploying Lambdas
=================

After finishing this course, you should know how to:
* How to package and deploy local Python code to AWS Lambda

## Overview

When developing code locally on your computer, it has to be packaged to be deployed
as a Lambda function. AWS Lambda supports
[deploying code as ZIP files](https://docs.aws.amazon.com/lambda/latest/dg/python-package.html)
or [container images](https://docs.aws.amazon.com/lambda/latest/dg/python-image.html).

> Note the following requirements for using a .zip file as your deployment package:
> The .zip file contains your function's code and any dependencies used to run your function's code (if applicable) on Lambda.
> If your function depends only on standard libraries, or AWS SDK libraries, you don't need to include these libraries
> in your .zip file. These libraries are included with the supported Lambda runtime environments.


## Excercise

1. Create the lambda function manually in the web console.
1. Save the source code of the function in a file, for example `function.py`.
1. Optionally, modify the file so it's behavior changes. For example, it can return a slightly different response.
1. Create a ZIP file containing the function file: `zip my-deployment-package.zip function.py`.
1. Deploy using `aws lambda update-function-code --function-name MyLambdaFunction --zip-file fileb://my-deployment-package.zip`
1. Call the function, either by uploading a file to S3, or making a HTTP request, depending on how you've set up the function triggers.
1. Verify that the function result includes the changes you made.

Now use the `requests` third-party library in your function to download an example JSON file, like `https://worldtimeapi.org/api/timezone/Europe/Warsaw`.
This will require to download and package it together with your function file. Follow the instructions
in the [AWS Lambda documentation](https://docs.aws.amazon.com/lambda/latest/dg/python-package.html#python-package-create-package-with-dependency).
