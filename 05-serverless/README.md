Serverless
==========

After finishing this course, you should know how to:
* How AWS Lambda allows to run applications without managing any servers
* How to deploy Python functions to AWS Lambda

## Overview

From the [AWS Lambda documentation](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html):

> Lambda is an ideal compute service for many application scenarios, as long as you can run your application code
> using the Lambda standard runtime environment and within the resources that Lambda provides.
>
> When using Lambda, you are responsible only for your code. Lambda manages the compute fleet that offers
> a balance of memory, CPU, network, and other resources to run your code. Because Lambda manages these resources,
> you cannot log in to compute instances or customize the operating system on provided runtimes.
> Lambda performs operational and administrative activities on your behalf, including managing capacity,
> monitoring, and logging your Lambda functions.

## Excercise

Create a Python function using the integrated developer environment, follow
the [AWS Lambda Python documentation](https://docs.aws.amazon.com/lambda/latest/dg/lambda-python.html):

1. Open the Lambda console
1. Choose Create function.
1. Configure the following settings:
   * Name – my-function.
   * Runtime – Python 3.9.
   * Role – Choose an existing role.
   * Existing role – lambda-role.
1. Choose Create function.
1. To configure a test event, choose Test.
1. For Event name, enter test.
1. Choose Save changes.
1. To invoke the function, choose Test.


* call the function by uploading files to an s3 bucket: https://docs.aws.amazon.com/lambda/latest/dg/with-s3-example.html
* call the function from an apigw, display request details: https://docs.aws.amazon.com/lambda/latest/dg/services-apigateway.html
* check logs
* monitor response times
