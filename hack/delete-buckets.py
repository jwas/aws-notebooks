#!/usr/bin/env python3

import argparse
import json

import boto3
import botocore

s3_client = boto3.client("s3")
version_paginator = s3_client.get_paginator("list_object_versions")
object_paginator = s3_client.get_paginator("list_objects_v2")


def main(args):
    response = s3_client.list_buckets()
    for bucket in response["Buckets"]:
        s3_objects = s3_client.list_objects_v2(Bucket=bucket["Name"])
        print(f"Checking bucket {bucket['Name']}")
        if args.no_dry_run:
            versioning = s3_client.get_bucket_versioning(Bucket=bucket["Name"])
            if versioning.get("Status") == "Enabled":
                s3_client.put_bucket_versioning(
                    Bucket=bucket["Name"],
                    VersioningConfiguration={"Status": "Suspended"},
                )
        response_iterator = version_paginator.paginate(Bucket=bucket["Name"])
        for response in response_iterator:
            versions = response.get("Versions", [])
            versions.extend(response.get("DeleteMarkers", []))
            keys = set()
            for key, version_id in [(x["Key"], x["VersionId"]) for x in versions]:
                keys.add(key)
                if version_id == "null":
                    continue
                print("Deleting {} version {}".format(key, version_id))
                if args.no_dry_run:
                    s3_client.delete_object(
                        Bucket=bucket["Name"], Key=key, VersionId=version_id
                    )
            # delete any remaining DeleteMarkers
            if args.no_dry_run:
                for key in keys:
                    print("Deleting {}".format(key))
                    s3_client.delete_object(
                        Bucket=bucket["Name"], Key=key, VersionId="null"
                    )

        # delete any unversioned objects
        response_iterator = object_paginator.paginate(Bucket=bucket["Name"])
        for response in response_iterator:
            if "Contents" in response:
                for obj in response.get("Contents"):
                    print("Deleting {}".format(obj["Key"]))
                    if args.no_dry_run:
                        s3_client.delete_object(Bucket=bucket["Name"], Key=obj["Key"])

        if args.no_dry_run:
            rm_bucket = s3_client.delete_bucket(Bucket=bucket["Name"])
            print(rm_bucket)

        print("")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--no-dry-run", action="store_true")
    args = parser.parse_args()
    main(args)
