#!/bin/bash

set -xeuo pipefail

registry=514111127510.dkr.ecr.eu-central-1.amazonaws.com

aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin $registry

name=$registry/jwas-lambda
version=$(git describe --tags --always)

docker build --tag "$name:$version" .

docker push "$name:$version"
