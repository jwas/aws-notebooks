Lambda images
=============

After finishing this course, you should know how to:
* How to build container images and deploy them as Lambda functions

## Overview

Building [container images](https://docs.aws.amazon.com/lambda/latest/dg/python-image.html)
with your application can make the process of deploying it easier and more repeatable.

A container can be easily tested locally: https://docs.aws.amazon.com/lambda/latest/dg/images-test.html#images-test-AWSbase

## Excercise

* Write a `Dockerfile` that would:
  * use the `public.ecr.aws/lambda/python:3.9` image as base
  * install the required third-party libraries from a `requirements.txt` file
  * copy your application source code
  * set a default entry point
* Create a repository for your application in a public Elastic Container Registry (ECR)
* Build the image and test it locally
* Push the image to the repository
* Create a Lambda using the image
* Make a change in the application and update the Lambda
* Test the result

