import json
import pprint

import requests


def handler(event, context):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(event)
    response = requests.get("https://worldtimeapi.org/api/timezone/Europe/Warsaw")
    pp.pprint(response.json())
    return ""
