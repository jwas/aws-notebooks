#!/bin/bash

set -xeuo pipefail

lambda=jwas-image
name=514111127510.dkr.ecr.eu-central-1.amazonaws.com/jwas-lambda
version=$(git describe --tags --always)

if ! aws lambda update-function-code --function-name "$lambda" --image-uri "$name:$version"; then
    aws lambda create-function --region eu-central-1 --function-name "$lambda" \
        --package-type Image \
        --code ImageUri="$name:$version"  \
        --role arn:aws:iam::514111127510:role/service-role/jwas-role-vt8tmmt9
fi
